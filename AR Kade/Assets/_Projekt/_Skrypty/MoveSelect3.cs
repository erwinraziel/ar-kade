﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveSelect3 : MonoBehaviour
{

    public float speed;
    Vector3 newPosition;
    Vector3 oldPosition;

    private float inputh;
    private float inputv;

    public Animator animator;
    bool allowmove;



    void Start()
    {
        animator = GetComponent<Animator>();

        oldPosition = Camera.main.WorldToScreenPoint(GameObject.FindGameObjectWithTag("Player").transform.position);
        newPosition = oldPosition;
        Debug.Log(oldPosition);
        allowmove = true;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GetDestination();
        }
        if (allowmove == true)
            MovePlayer();


    }

    void GetDestination()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            newPosition = hit.point;
            newPosition.y = 0;
            Debug.Log(hit.point);
        }

    }

    void StopPlayer()
    {
        oldPosition = transform.position;
        newPosition = oldPosition;
        animator.Play("Idle", -1);
    }

    void MovePlayer()
    {

        if (transform.position != newPosition)
        {
            animator.Play("run", -1);
            transform.LookAt(newPosition);
            transform.position = Vector3.MoveTowards(transform.position, newPosition, speed * Time.deltaTime);
        }
        else
        {
            animator.Play("Idle", -1);

        }
    }

    void OnCollisionEnter(Collision other)
    {

        Debug.Log("duckey");
    }


}