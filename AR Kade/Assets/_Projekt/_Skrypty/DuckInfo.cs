﻿using UnityEngine;
using System.Collections;

public class DuckInfo : MonoBehaviour {

    public float width;
    public float height;
    private string sct;
    //public GameObject scoreRead;
    private int sc;
    public int cntr;
    // Use this for initialization
    void Start ()
    {
        sc = 0;
        InvokeRepeating("addScore", 0, 1);
        InvokeRepeating("retScore", 0, 2);
    }
	
	// Update is called once per frame

    void OnGUI()
    {
        GUI.Box(new Rect(0, 0, Screen.width * width, Screen.height * height), "menu");

        if (GUI.Button(new Rect(0, Screen.height * height * 1 / 5, Screen.width * width,
            Screen.height * height * 1 / 5), "photo"))
            Application.CaptureScreenshot("" + Time.time + ".jpg");

        if (GUI.Button(new Rect(0, Screen.height * height * 2 / 5, Screen.width * width,
            Screen.height * height * 1 / 5), "menu"))
            Application.LoadLevel("SelectScene");

        if (GUI.Button(new Rect(0, Screen.height * height * 3 / 5, Screen.width * width,
            Screen.height * height * 1 / 5), "reload"))
        {
            Application.LoadLevel(Application.loadedLevel);
            sc = 0;
        }

        GUI.TextArea(new Rect(0, Screen.height * height * 4 / 5, Screen.width * width,
            Screen.height * height * 1 / 5), sct);


        
    }

    public void addScore()
    {
        sc += cntr;
    }

    public void retScore()
    {
        Debug.Log(sc);
        sct = sc.ToString();
    }
}
