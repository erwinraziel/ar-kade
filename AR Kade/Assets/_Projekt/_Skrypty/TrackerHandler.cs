﻿using UnityEngine;
using System.Collections;
using Vuforia;

/// <summary>
/// Detekcja markerów dla pozostałych scen.
/// </summary>
public class TrackerHandler : MonoBehaviour, 
							  ITrackableEventHandler

{
	private TrackableBehaviour vTrack;

		void Start () 
		{
			vTrack = GetComponent<TrackableBehaviour>();
			if (vTrack) vTrack.RegisterTrackableEventHandler(this);
		}
	
		public void OnTrackableStateChanged(
			TrackableBehaviour.Status previousStatus,
			TrackableBehaviour.Status newStatus)
		{
			
			if (newStatus == TrackableBehaviour.Status.DETECTED ||
				newStatus == TrackableBehaviour.Status.TRACKED ||
				newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
				
				TrackerFound();	
		
			else 
				TrackerLost();
		}
		
		private void TrackerFound()
		{
			Renderer[] renderers = GetComponentsInChildren<Renderer>(true);
			Collider[] colliders = GetComponentsInChildren<Collider>(true);

			foreach (Renderer component in renderers) component.enabled = true;
			foreach (Collider component in colliders) component.enabled = true;

			Debug.Log("Track " + vTrack.TrackableName + " found");
			if (transform.Find("Controls") != null)
			transform.Find ("Controls").GetComponent<CanvasGroup> ().alpha = 1f;
			}

		private void TrackerLost()
		{
			Renderer[] renderers = GetComponentsInChildren<Renderer>(true);
			Collider[] colliders = GetComponentsInChildren<Collider>(true);
			foreach (Renderer component in renderers) component.enabled = false;
			foreach (Collider component in colliders) component.enabled = false;

			Debug.Log("Track " + vTrack.TrackableName + " lost");
			if (transform.Find("Controls") != null)	
			transform.Find ("Controls").GetComponent<CanvasGroup> ().alpha = 0f;
		}
}

