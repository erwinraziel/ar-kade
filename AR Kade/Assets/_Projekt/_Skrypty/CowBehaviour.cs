﻿using UnityEngine;
using System.Collections;

public class CowBehaviour : MonoBehaviour {

    private GameObject Target;
    private Vector3 location;
    public float health;
    public float precision;
    public float speed;
    // Use this for initialization
    void Start () {
        Target = GameObject.FindGameObjectWithTag("Player");
        InvokeRepeating("die", health, 0);
    }
	
	// Update is called once per frame
	void Update () {
        moveCow();
	}

    void moveCow()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - Target.transform.position);
        if (Vector3.Distance(transform.position, Target.transform.position) > 25)
        {
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, speed * Time.deltaTime);

            //die();
            //transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, speed * Time.deltaTime);
        }
        else
            Application.LoadLevel(Application.loadedLevel);
            Debug.Log("lost");

    }

    void die()
    {
        Destroy(gameObject);
    }

}
