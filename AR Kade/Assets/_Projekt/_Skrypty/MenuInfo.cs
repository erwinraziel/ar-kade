﻿using UnityEngine;
using System.Collections;

public class MenuInfo : MonoBehaviour {

    public float width;
    public float height;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnGUI()
    {
        GUI.Box(new Rect(0, 0, Screen.width*width, Screen.height*height), "menu");

        if (GUI.Button(new Rect(0, Screen.height * height * 1 / 4, Screen.width * width,
            Screen.height * height * 1 / 4), "photo"))
            Application.CaptureScreenshot(""+Time.time+".jpg");

        if (GUI.Button(new Rect(0, Screen.height * height * 2 / 4, Screen.width * width,
            Screen.height * height * 1 / 4), "reload"))
            Application.LoadLevel("SelectScene");

        //GUI.Button(new Rect(0, Screen.height * height * 3 / 5, Screen.width * width, Screen.height * height * 1 / 5), "exit");

    }
}
