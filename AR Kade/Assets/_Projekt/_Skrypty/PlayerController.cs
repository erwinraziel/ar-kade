﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Sterowanie i animacja w minigrze na markerze.
/// Obsługuje sterowanie postacią i "zbieranie" punktów.
/// </summary>
public class PlayerController : MonoBehaviour {

	public int score;
	public int max;
	public List<Transform> collectible = new List<Transform>();

	public float speed = 800.0f;

	void Start () 
	{
		score = 0;
		foreach (Transform t in GameObject.Find("TargetV2").transform)
			if (t.tag == "collect") {
				collectible.Add (t);
			}
		max = collectible.Count;
	}

	void FixedUpdate () 
	{
	}
		
	public void ButtonLeft_Press()
	{
		Vector3 movement = new Vector3 (-1, 0.0f, 0);
		GetComponent<Rigidbody> ().AddForce (movement * speed * 30 * Time.deltaTime);
	}

	public void ButtonRight_Press()
	{
		Vector3 movement = new Vector3 (1, 0.0f, 0);
		GetComponent<Rigidbody> ().AddForce (movement * speed * 30 * Time.deltaTime);
	}

	public void ButtonDown_Press()
	{
		Vector3 movement = new Vector3 (0, 0.0f, -1);
		GetComponent<Rigidbody> ().AddForce (movement * speed * 30 * Time.deltaTime);
	}

	public void ButtonUp_Press()
	{
		Vector3 movement = new Vector3 (0, 0.0f, 1);
		GetComponent<Rigidbody> ().AddForce (movement * speed * 30 * Time.deltaTime);
	}

	//zdobycie punktu
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "collect") 
		{
			Pointup ();
			other.gameObject.SetActive (false);
		}
	}

	void Pointup()
	{
		score++;
		Debug.Log (score);
		GameObject.FindWithTag("Finish").GetComponent<TextMesh> ().text = score + "/" + max;
	}
}
