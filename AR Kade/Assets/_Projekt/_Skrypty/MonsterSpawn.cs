﻿using UnityEngine;
using System.Collections;

public class MonsterSpawn : MonoBehaviour {

    // Use this for initialization
    public float spawnTime;
    public Transform[] spawnPoints;
    public Transform[] Enemies;

    void Start () {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }
	
	// Update is called once per frame
	void Spawn() {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        int EnemiesIndex = Random.Range(0, Enemies.Length);
        Instantiate(Enemies[EnemiesIndex], spawnPoints[spawnPointIndex].position, 
            spawnPoints[spawnPointIndex].rotation);
    }
}
