﻿using UnityEngine;
using System.Collections;

public class SelectMoveLow : MonoBehaviour {

    public float precisionMove;
    public float speed;
    private Vector3 newPosition;
//private Vector3 oldPosition;

    public Animator animat;
    //bool allowMove;

    public GameObject Level1;
    public GameObject Level2;
    public GameObject Level3;
    public GameObject LevelExit;
    public GameObject LevelOpt;

    public string Level1Name;
    public string Level2Name;
    public string Level3Name;
    public string LevelOptName;




    // Use this for initialization
    void Start () {

        newPosition = transform.position;
        //Debug.Log("oldpos " + oldPosition);
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            getDestination();
        }
        movePlayer();
        checkSelection();
    }

    void getDestination()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 10000))
        {
            if (hit.collider.tag != "Player")
                newPosition = new Vector3(hit.point.x, hit.point.y, hit.point.z);
        }
    }

    void movePlayer()
    {
        if (Vector3.Distance(transform.position, newPosition) > precisionMove)
        {
            animat.Play("run", -1);
            transform.LookAt(newPosition);
            transform.position = Vector3.MoveTowards(transform.position, newPosition, speed * Time.deltaTime);
        }
        else
        {
            animat.Play("idle", -1);
        }
    }

    void checkSelection()
    {
        if (Vector3.Distance(transform.position, Level1.transform.position) < precisionMove)
            Application.LoadLevel(Level1Name);
        if (Vector3.Distance(transform.position, Level2.transform.position) < precisionMove)
            Application.LoadLevel(Level2Name);
        if (Vector3.Distance(transform.position, Level3.transform.position) < precisionMove)
            Application.LoadLevel(Level3Name);
        if (Vector3.Distance(transform.position, LevelOpt.transform.position) < precisionMove)
            Application.LoadLevel(LevelOptName);
        if (Vector3.Distance(transform.position, LevelExit.transform.position) < precisionMove)
            Application.Quit();
    }

}
