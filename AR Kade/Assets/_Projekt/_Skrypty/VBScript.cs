﻿using UnityEngine;
using System.Collections;
using Vuforia;


public class VBScript : MonoBehaviour, IVirtualButtonEventHandler {

    GameObject vb1;
    GameObject vb2;
    GameObject vb3;
    GameObject vb4;
    GameObject vb5;
    GameObject vb6;
    GameObject vb7;
    GameObject vb8;

    //public CharacterController controller;
    public GameObject player;
    private Animator animator;



    // Use this for initialization
    void Start () {

        animator = player.GetComponent<Animator>();
        animator.Play("idle");

        vb1 = transform.Find("VB1").gameObject;
        vb2 = transform.Find("VB2").gameObject;
        vb3 = transform.Find("VB3").gameObject;
        vb4 = transform.Find("VB4").gameObject;
        vb5 = transform.Find("VB5").gameObject;
        vb6 = transform.Find("VB6").gameObject;
        vb7 = transform.Find("VB7").gameObject;
        vb8 = transform.Find("VB8").gameObject;


        VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();

        for (int i = 0; i < vbs.Length; ++i)
        {
            vbs[i].RegisterEventHandler(this);
        }
    }

    void update()
    {
        
    }
    
    public void tap(GameObject vbx)
    {
        vbx.SetActive(false);
    }

    public void untap()
    {
        vb1.SetActive(true);
        vb2.SetActive(true);
        vb3.SetActive(true);
        vb4.SetActive(true);
        vb5.SetActive(true);
        vb6.SetActive(true);
        vb7.SetActive(true);
        vb8.SetActive(true);

    }

    // Update is called once per frame

    public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
    {
        switch (vb.VirtualButtonName)
        {

            case "VB1":
                {
                    Debug.Log("1");
                    
                    //moveToPosition();
                    animator.Play("idle1");
                    tap(vb1);
                }
                break;

            case "VB2":
                {
                    Debug.Log("2");
                    animator.Play("dance1");
                   tap(vb2);
                }
                break;

            case "VB3":
                {
                    Debug.Log("3");
                    animator.Play("dance2");
                   // tap(vb3);
                }
                break;

            case "VB4":
                {
                    Debug.Log("4");
                    animator.Play("cheer");
                    tap(vb4);
                }
                break;

            case "VB5":
                {
                    Debug.Log("5");
                    animator.Play("jump1");
                    tap(vb5);
                }
                break;

            case "VB6":
                {
                    Debug.Log("damaged1");
                    animator.Play("damaged1");
                   tap(vb6);
                }
                break;

            case "VB7":
                {
                    Debug.Log("7");
                    animator.Play("damaged2");
                    tap(vb7);
                }
                break;

            case "VB8":
                {
                    Debug.Log("8");
                    animator.Play("lose");
                    Application.LoadLevel("SelectScene");
                    tap(vb8);
                }
                break;
        }
    }

    public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
    {
        Debug.Log("np");
        untap();
    }
}
