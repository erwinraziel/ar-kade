﻿using UnityEngine;
using System.Collections;

public class DuckBehaviour : MonoBehaviour {

    private GameObject Target;
    private Vector3 location;
    public static int duckScore;
    private Vector3 destination;
    private GameObject Defender;

    public float precision;
    private GameObject scoreReader;
    private float speed;
    private bool allowMove;
    private int health;

    public GameObject based;


    void Start ()
    {
        speed = Random.Range(12, 30);
        health = 1;
        location = transform.position;
        Target = GameObject.FindGameObjectWithTag("DefendPoint");
        Defender = GameObject.FindGameObjectWithTag("Player");
	}

	void Update () {
        moveDuck();
        //checkDuck();
    }

    void moveDuck()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - Target.transform.position);

        if (Vector3.Distance(transform.position, Defender.transform.position) < 50)
        {
            die();
            //transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, speed * Time.deltaTime);
        }

        if (Vector3.Distance(transform.position, Target.transform.position)>precision)
        {
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, speed * Time.deltaTime);
        }

        else
        {
            Debug.Log("lost");
            Application.LoadLevel(Application.loadedLevel);
        }        
    }

    void die()
    {
        Destroy(gameObject);
    }
}
