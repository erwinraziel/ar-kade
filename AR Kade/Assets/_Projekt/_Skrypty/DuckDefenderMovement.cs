﻿using UnityEngine;
using System.Collections;

public class DuckDefenderMovement : MonoBehaviour {

    public float speed;
    public float precisionMove;
    public CharacterController controller;
    private Vector3 position;
    private bool order;

    public Animator animator;

    // Use this for initialization
    void Start () {
        position = transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButton(0))
        {
            locatePosition();
        }
        moveToPosition();

    }

    void locatePosition()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000))
        {
            if (hit.collider.tag != "Player")
                position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
        }
    }

    void moveToPosition()
    {
        if (Vector3.Distance(transform.position, position) > precisionMove)
        {
            Quaternion newRotation = Quaternion.LookRotation(position - transform.position); //vector.forward powodowal problemy z ruchem

            newRotation.x = 0;
            newRotation.z = 0;

            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 10);
            controller.SimpleMove(transform.forward * speed);

            animator.Play("run");
        }
        else
        {
            animator.Play("idle");
        }


    }


}
